﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestSuite
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestSuiteSimplePasses()
        {
            // Use the Assert class to test conditions
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator TestSuiteWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }

        private SaberGame game;
        [UnityTest]
        public IEnumerator TestBlockGame() {
            GameObject go = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/RedBlockSlicable"));
            float initialYPos = go.transform.position.y;
            yield return new WaitForSeconds(0.1f);
            Assert.AreEqual(initialYPos, go.transform.position.y);
        }

        [UnityTest]
        public IEnumerator TestIfBlockHasBeenCreated() {
            GameObject go = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/RedBlockSlicable"));
            yield return new WaitForSeconds(0.1f);
            Assert.IsNotNull(go);
        }        

    }
}
